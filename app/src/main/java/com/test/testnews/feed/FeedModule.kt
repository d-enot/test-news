package com.test.testnews.feed

import androidx.lifecycle.ViewModel
import com.test.testnews.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class FeedModule {

    @ContributesAndroidInjector internal abstract fun feedsFragment(): FeedFragment

    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel::class)
    abstract fun bindFeedModel(feedViewModel: FeedViewModel): ViewModel
}