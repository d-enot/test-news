package com.test.testnews.feed

import androidx.databinding.ObservableField
import com.test.testnews.BaseViewModel
import com.test.testnews.newsfeeds.domain.Feed
import javax.inject.Inject

class FeedViewModel @Inject constructor() : BaseViewModel() {

    val title = ObservableField<String>()

    val article = ObservableField<String>()

    fun bindView(feed: Feed) {
        title.set(feed.title)
        article.set(feed.storyTxt)
    }
}