package com.test.testnews.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.test.testnews.ARGUMENT_FEED
import com.test.testnews.R
import com.test.testnews.databinding.FragmentFeedBinding
import com.test.testnews.di.ActivityScoped
import com.test.testnews.di.FragmentScoped
import com.test.testnews.newsfeeds.domain.Feed
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@ActivityScoped
class FeedFragment @Inject constructor(): DaggerFragment() {

    @Inject lateinit var viewModel: FeedViewModel
    @BindView(R.id.img) lateinit var img: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil
            .inflate<FragmentFeedBinding>(inflater, R.layout.fragment_feed, container, false)
        ButterKnife.bind(this, binding.root)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val feed: Feed? = arguments?.getParcelable(ARGUMENT_FEED)
        feed?.let {
            setImg(it)
            viewModel.bindView(it)
        }
    }

    private fun setImg(feed: Feed) {
        context?.let {
            Glide.with(it).load(feed.img).apply(RequestOptions.centerCropTransform()).into(img)
        }
    }

    companion object {
        fun newInstance() {
            //put some code here
        }
    }
}