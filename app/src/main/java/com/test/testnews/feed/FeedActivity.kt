package com.test.testnews.feed

import android.os.Bundle
import com.test.testnews.R
import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class FeedActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var fragmentProvider: Lazy<FeedFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)
        supportFragmentManager?.beginTransaction()?.add(R.id.frame, fragmentProvider.get(), "feed")?.commit()
    }
}