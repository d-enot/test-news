package com.test.testnews.source

import com.test.testnews.newsfeeds.domain.Feed
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FeedsRepository @Inject constructor(var remoteDataSource: FeedsDataSource) : FeedsDataSource {

    private var cachedFeeds: MutableList<Feed>? = null
    private var cacheIdDirty = true

    override fun getFeeds(page: Int, size: Int): Single<List<Feed>> {
        return if (cacheIdDirty && cachedFeeds == null) {
            getAndSaveFeeds(page, size)
        } else {
            return Single.just(cachedFeeds)
        }
    }

    private fun getAndSaveFeeds(page: Int, size: Int) = remoteDataSource.getFeeds(page, size)
        .doOnSuccess { list ->
            cachedFeeds = list as MutableList<Feed>
            cacheIdDirty = false
        }
}