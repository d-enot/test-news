package com.test.testnews.source.remote

import com.test.testnews.network.Api
import com.test.testnews.newsfeeds.domain.Feed
import com.test.testnews.source.FeedsDataSource
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FeedsRemoteDataSource @Inject constructor(var api: Api) : FeedsDataSource {

    override fun getFeeds(page: Int, size: Int): Single<List<Feed>> {
        return api.getFeeds().map { result -> result.feeds }
    }

}