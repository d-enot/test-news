package com.test.testnews.source

import com.test.testnews.newsfeeds.domain.Feed
import io.reactivex.Single

interface FeedsDataSource {

    fun getFeeds(page: Int, size: Int): Single<List<Feed>>
}