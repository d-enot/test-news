package com.test.testnews

const val MAIN_URL = "https://content.guardianapis.com/"
const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss Z"
const val CACHE_SIZE = 20 * 1024 * 1024L
const val REQUEST_TIMEOUT = 30L
const val ARGUMENT_FEED = "argument_feed"

