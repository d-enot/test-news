package com.test.testnews.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.test.testnews.*
import com.test.testnews.network.Api
import com.test.testnews.newsfeeds.domain.Feed
import com.test.testnews.newsfeeds.domain.FeedsResponse
import com.test.testnews.source.FeedsDataSource
import com.test.testnews.source.remote.FeedsRemoteDataSource
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModuleProviders {

    @Provides
    @Singleton
    fun provideHttpClient(cachedDir: File): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.cache(Cache(cachedDir, CACHE_SIZE))
        httpClientBuilder.readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        httpClientBuilder.addInterceptor(interceptor)
        return httpClientBuilder.build()
    }

    @Provides
    @Singleton
    fun getMapper(): Gson {
        return GsonBuilder().setDateFormat(DATE_FORMAT)
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
            .registerTypeAdapter(FeedsResponse::class.java, FeedsDeserializer())
            .create()
    }

    @Provides
    @Singleton
    fun getCacheDir(context: Context): File {
        val external = context.externalCacheDir
        return external ?: context.cacheDir
    }

    @Provides
    @Singleton
    fun providesApi(httpClient: OkHttpClient, mapper: Gson): Api {
        return Retrofit.Builder()
            .client(httpClient)
            .baseUrl(MAIN_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(mapper))
            .build()
            .create(Api::class.java)
    }

    @Provides
    @Singleton
    fun remoteDataSource(api: Api): FeedsDataSource = FeedsRemoteDataSource(api)
}