package com.test.testnews.di

import com.test.testnews.feed.FeedActivity
import com.test.testnews.feed.FeedModule
import com.test.testnews.newsfeeds.FeedsActivity
import com.test.testnews.newsfeeds.FeedsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FeedsModule::class])
    abstract fun feedsActivity(): FeedsActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FeedModule::class])
    abstract fun feedActivity(): FeedActivity
}