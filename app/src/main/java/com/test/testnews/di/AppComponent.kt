package com.test.testnews.di

import android.app.Application
import com.test.testnews.App
import com.test.testnews.feed.FeedModule
import com.test.testnews.newsfeeds.FeedsModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AppModuleProviders::class,
    ActivityBindingModule::class,
    ViewModelFactoryModule::class,
    AndroidSupportInjectionModule::class]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(application: App)

    override fun inject(instance: DaggerApplication?)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}