package com.test.testnews.network

import com.test.testnews.newsfeeds.domain.FeedsResponse
import io.reactivex.Single
import retrofit2.http.GET

interface Api {

    @GET("search?format=json&show-tags=contributor" +
            "&show-fields=starRating,headline,thumbnail,body,lastModified" +
            "&show-refinements=all&order-by=newest" +
            "&api-key=d9f56ff2-0d7e-45d1-90b1-89a143ff0d5b")
    fun getFeeds(): Single<FeedsResponse>
}