package com.test.testnews

interface UseCase<in Q: Params, out T> {

    fun execute(params: Q): T
}

interface Params