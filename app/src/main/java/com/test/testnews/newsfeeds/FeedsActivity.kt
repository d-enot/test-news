package com.test.testnews.newsfeeds

import android.os.Bundle
import com.test.testnews.R
import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class FeedsActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var fragmentProvider: Lazy<FeedsFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feeds)
        supportFragmentManager?.beginTransaction()?.add(R.id.frame, fragmentProvider.get(), "feeds")?.commit()
    }
}