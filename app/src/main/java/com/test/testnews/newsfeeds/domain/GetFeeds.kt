package com.test.testnews.newsfeeds.domain

import com.test.testnews.Params
import com.test.testnews.UseCase
import com.test.testnews.di.ActivityScoped
import com.test.testnews.source.FeedsRepository
import io.reactivex.Single
import javax.inject.Inject

@ActivityScoped
class GetFeeds @Inject constructor(var repository: FeedsRepository
) : UseCase<GetFeedsParams, Single<List<Feed>>> {

    override fun execute(params: GetFeedsParams): Single<List<Feed>> = repository.getFeeds(params.page, params.size)
}

class GetFeedsParams(val page: Int, val size: Int): Params