package com.test.testnews.newsfeeds

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions.centerCropTransform
import com.test.testnews.R
import com.test.testnews.databinding.ItemFeedBinding
import com.test.testnews.newsfeeds.domain.Feed


class FeedsAdapter(private val items: MutableList<Feed> = mutableListOf(), val viewModel: FeedsViewModel)
    : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemFeedBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_feed,
            parent,
            false)
        return ViewHolder(binding.root, parent.context, binding, viewModel, items)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feed = items.get(position)
        holder.bindView(FeedItemViewModel(), feed)
    }

    fun setData(data: List<Feed>) {
        items.addAll(data)
        notifyDataSetChanged()
    }
}

class ViewHolder(view: View,
                 val context: Context,
                 val binding: ItemFeedBinding,
                 val viewModel: FeedsViewModel,
                 val items: MutableList<Feed>)
    : RecyclerView.ViewHolder(view), View.OnClickListener {

    @BindView(R.id.img) lateinit var img: ImageView

    init {
        ButterKnife.bind(this, view)
        view.setOnClickListener(this)
    }

    fun bindView(viewModel: FeedItemViewModel, feed: Feed) {
        binding.viewmodel = viewModel
        viewModel.bindItem(feed)
        Glide.with(context).load(feed.img).apply(centerCropTransform()).into(img)
    }

    override fun onClick(v: View?) {
        viewModel.onItemClick.onNext(items[layoutPosition])
    }
}
