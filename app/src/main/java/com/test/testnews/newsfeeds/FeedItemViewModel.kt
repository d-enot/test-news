package com.test.testnews.newsfeeds

import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import com.test.testnews.newsfeeds.domain.Feed
import org.joda.time.LocalDate

class FeedItemViewModel : BaseObservable() {

    val category = ObservableField<String>()

    val date = ObservableField<String>()

    val title = ObservableField<String>()

    val article = ObservableField<String>()

    val img = ObservableField<String>()

    fun bindItem(feed: Feed) {
        category.set(feed.category)
        date.set(LocalDate(feed.date).toString())
        title.set(feed.title)
        article.set(feed.storyTxt)
    }
}