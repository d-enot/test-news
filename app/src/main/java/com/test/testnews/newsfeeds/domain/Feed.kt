package com.test.testnews.newsfeeds.domain

import android.os.Parcel
import android.os.Parcelable

data class Feed(val date: Long,
                val title: String,
                val img: String,
                val category: String,
                val storyTxt: String,
                var offline: Boolean = false,
                var pined: Boolean = false) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(date)
        parcel.writeString(title)
        parcel.writeString(img)
        parcel.writeString(category)
        parcel.writeString(storyTxt)
        parcel.writeByte(if (offline) 1 else 0)
        parcel.writeByte(if (pined) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Feed> {
        override fun createFromParcel(parcel: Parcel): Feed {
            return Feed(parcel)
        }

        override fun newArray(size: Int): Array<Feed?> {
            return arrayOfNulls(size)
        }
    }
}

data class FeedsResponse(val feeds: List<Feed>)
