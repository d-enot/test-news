package com.test.testnews.newsfeeds

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.test.testnews.ARGUMENT_FEED
import com.test.testnews.R
import com.test.testnews.di.ActivityScoped
import com.test.testnews.feed.FeedActivity
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@ActivityScoped
class FeedsFragment @Inject constructor() : DaggerFragment() {

    @Inject lateinit var viewModel: FeedsViewModel
    @BindView(R.id.feeds) lateinit var feedsList: RecyclerView
    private lateinit var feedsAdapter: FeedsAdapter
    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_feeds, container, false)
        ButterKnife.bind(this, rootView)

        initList()
        subscribeView()
        return rootView
    }

    override fun onResume() {
        super.onResume()
        viewModel.getFeeds()
    }

    private fun subscribeView() {
        disposable.add(viewModel
            .loadSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {list -> feedsAdapter.setData(list)}, { error -> error.printStackTrace()}))

        disposable.add(viewModel.onItemClick.subscribe { feed ->
            val bundle = Bundle()
            bundle.putParcelable(ARGUMENT_FEED, feed)
            activity?.startActivity(Intent(context, FeedActivity::class.java), bundle)
            disposable.dispose()
        })
    }

    private fun initList() {
        feedsAdapter = FeedsAdapter( viewModel = viewModel)
        feedsList.apply {
            adapter = feedsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}