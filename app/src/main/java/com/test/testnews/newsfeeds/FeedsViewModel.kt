package com.test.testnews.newsfeeds

import androidx.lifecycle.ViewModel
import com.test.testnews.di.ActivityScoped
import com.test.testnews.newsfeeds.domain.Feed
import com.test.testnews.newsfeeds.domain.GetFeeds
import com.test.testnews.newsfeeds.domain.GetFeedsParams
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@ActivityScoped
class FeedsViewModel @Inject constructor(var getFeeds: GetFeeds) : ViewModel() {

    var loadSubject: BehaviorSubject<List<Feed>> = BehaviorSubject.create()
    var onItemClick: PublishSubject<Feed> = PublishSubject.create()

    fun getFeeds() {
        val params = GetFeedsParams(1, 15)
        getFeeds.execute(params)
            .doOnSuccess { list -> loadSubject.onNext(list) }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }
}