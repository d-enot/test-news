package com.test.testnews.newsfeeds

import androidx.lifecycle.ViewModel
import com.test.testnews.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class FeedsModule {

    @ContributesAndroidInjector internal abstract fun feedsFragment(): FeedsFragment

    @Binds
    @IntoMap
    @ViewModelKey(FeedsViewModel::class)
    abstract fun bindMyViewModel(feedsViewModel: FeedsViewModel): ViewModel
}