package com.test.testnews

import android.os.Build
import android.text.Html
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.test.testnews.newsfeeds.domain.Feed
import com.test.testnews.newsfeeds.domain.FeedsResponse
import org.joda.time.DateTime
import java.lang.reflect.Type

class FeedsDeserializer : JsonDeserializer<FeedsResponse> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): FeedsResponse {

        val mainData = json?.asJsonObject?.getAsJsonObject("response")
        val results = mainData?.getAsJsonArray("results")
        var feeds = mutableListOf<Feed>()
        results?.forEach { i ->
            var rawFeed = i.asJsonObject
            var rawFeedsFields = rawFeed.getAsJsonObject("fields")
            var time = DateTime(rawFeed.get("webPublicationDate").asString)
            var category = rawFeed.get("pillarName")?.asString ?: "Inside track"
            var article = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(rawFeedsFields.get("body").asString, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(rawFeedsFields.get("body").asString)
            }
            var feed = Feed(time.millis,
                rawFeed.get("webTitle").asString,
                rawFeedsFields.get("thumbnail").asString,
                category,
                article.toString()
            )
            feeds.add(feed)
        }
        return FeedsResponse(feeds)
    }
}